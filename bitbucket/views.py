from django.shortcuts import render
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.response import Response

import git, os


# Create your views here.

class BitBucket(APIView):

    BASE_DIR='/var/www/dev.qband.com'

    def post(self, request, formst=None):
        #
        # Check that this is actually from bitbucket
        #
        # Todo: Expand to run unittests and to roll back the changes. Also should be moved to a seperate server maybe.

        print "BitBucket Webhook"
        print "Remote Host:", request.META['REMOTE_HOST']
        print "  Remote IP:", request.META['REMOTE_ADDR']

        print "Updating (%s)" % request.data['repository']['name']
        g = git.cmd.Git(os.path.join(self.BASE_DIR, request.data['repository']['name']))
        g.pull()
        print "Completed"
        if request.data['repository']['name'] == 'qband':
            print "This is a backend update, we might need to imstall requirements and update the database."
            from pip.req import InstallRequirement
            print "- Running through python requirements."
            for line in open(os.path.join(self.BASE_DIR, request.data['repository']['name'], "src","requirements.txt")):
                req = InstallRequirement.from_line(line, None)
                print "  ",req
            print "- Database structure"

            #from pip.commands.install import InstallCommand
            #command = InstallCommand()
            #command.




        return Response({'message': 'Success'}, status=200)
