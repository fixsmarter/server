from django.conf.urls import include, url

from views import BitBucket

urlpatterns = [
    url(r'^$', BitBucket.as_view()),
]