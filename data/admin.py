from django.contrib import admin

# Register your models here.

from models import *

admin.site.register(Term)
admin.site.register(Result)
admin.site.register(Search)
admin.site.register(ProviderStatus)
admin.site.register(Provider)
admin.site.register(PaymentOptions)

