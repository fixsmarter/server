from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db import models
from django.db.models.signals import post_save, pre_delete

# -------------------------------------------------------------------
# Data API related models
# -------------------------------------------------------------------

class Term(models.Model):
    terms = models.CharField(max_length=255, unique=True)
    score = models.IntegerField(default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s - %s" % (self.terms, self.score)

#
# Search definition
#
class Search(models.Model):
    token = models.CharField(max_length=40, unique=True)
    terms = models.CharField(max_length=255)
    saved = models.BooleanField(default=False)
    user = models.ManyToManyField(User, related_name='searches')


    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s - %s" % (self.token, self.terms)
#
# Result for a search.
#
class Result(models.Model):

    provider = models.CharField(max_length=255, default='')
    matched_terms = models.CharField(max_length=255, default='')
    title = models.CharField(max_length=255, default='')
    description = models.TextField(default='')
    url = models.CharField(max_length=255, default='')
    image = models.CharField(max_length=255, default='')
    video = models.CharField(max_length=255, default='')
    search = models.ForeignKey(Search, related_name='results', null=True)
    returned = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s - %s - %s" % (self.search.token, self.provider, self.title)
#
# Status of the job for a specific search.
#
class ProviderStatus(models.Model):
    name = models.CharField(max_length=20)
    job = models.CharField(max_length=40)
    search = models.ForeignKey(Search, related_name='provider_jobs', null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s - %s" % (self.name, self.job)

#
# List of providers and the status.
#
class Provider(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField(blank=True)
    url = models.URLField(blank=True)
    image = models.URLField(blank=True)
    enabled = models.BooleanField(default=True)


    def __str__(self):
        return self.name

#
# Profile and Advertiser related models
#
class PaymentOptions(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name


class IndustryOptions(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name


class Payments(models.Model): #Place Holder
    pass


class UserProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True, related_name='profile')
    company = models.CharField(max_length=100, blank=True)
    url = models.URLField(blank=True)
    phone = models.CharField(max_length=15, blank=True)
    payment = models.ManyToManyField(PaymentOptions)
    industry = models.ManyToManyField(IndustryOptions)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.user.username

    #
    # These are events we catch in order to keep our profile sane and clean up when we are done.
    #
    @receiver(post_save, sender=User)
    def create_profile_for_user(sender, instance=None, created=False, **kwargs):
        if created:
            UserProfile.objects.get_or_create(user=instance)

    @receiver(pre_delete, sender=User)
    def delete_profile_for_user(sender, instance=None, **kwargs):
        if instance:
            user_profile = UserProfile.objects.get(user=instance)
            user_profile.delete()
