from __future__ import absolute_import

from server.celery import app
from providers.provider import *
from data.models import Provider, Term
import requests, json, datetime, logging


@app.task
def task_expire_results():
    pass


@app.task(bind=True)
def task_solr_populate(self):
    logging.info("Hot Terms v1.0 ;)")
    term_date =  datetime.datetime.now()
    for counter in range(1,2):

        logging.info("- Connecting to Google and getting terms for %s" % term_date)
        response = requests.get("http://www.google.com/trends/hottrends/hotItems?ajax=1&htd=%s&pn=p1&htv=l" % term_date.strftime("%Y%m%d"))
        results = response.json()
        for result in results['trendsByDateList']:
            for trend in result['trendsList']:
                logging.info("Updating the database")
                try:
                    term = Term(terms=trend['title'], score=trend['hotnessLevel'])
                    term.save()
                except Exception, e:
                    term = Term.objects.get(terms=trend['title'])
                    term.score = score=trend['hotnessLevel']
                logging.info("  - Getting results for %s" % trend['title'])
                for provider in Provider.objects.all().filter(enabled=True):
                    try:
                        _provider = eval("%sProvider()" % provider.name)
                        if _provider.authenticate():
                            _provider.update_solr(trend['title'])
                            logging.info("    - %s: OK" % provider.name)
                        else:
                            logging.info("    - %s: Authentication Failed" % provider.name)
                    except Exception, e:
                        #print e
                        logging.info("    - %s: Method Failed" % provider.name)
                        logging.exception(e)
                        #Todo: Should be better
        term_date -= datetime.timedelta(days=1)
