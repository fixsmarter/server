from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token
from django.conf.urls import include, url
from django.views.decorators.csrf import csrf_exempt

from . import views


router = DefaultRouter()
#router.register(r'profiles', views.ProfileViewSet)
#router.register(r'notification', views.NotificationAPIView)
#router.register(r'users', views.UserViewSet)

urlpatterns = [
    url(r'^auth/token/', obtain_auth_token, name='api-token'),
    url(r'^auth/', include('rest_auth.urls')),
    url(r'^auth/registration/', include('rest_auth.registration.urls')),
    url(r'^auth/facebook/$', views.FacebookLogin.as_view(), name='facebook-login'),
    url(r'^auth/google/$', views.GoogleLogin.as_view(), name='google-login'),
    url(r'^auth/twitter/$', views.TwitterLogin.as_view(), name='twitter-login'),
    url(r'^auth/linkedin/$', views.LinkedInLogin.as_view(), name='linkedin-login'),

    url(r'^data/search/$', csrf_exempt(views.DataSearch.as_view()), name='data-search'), #Todo: Remove csrf_excempt before we go life.
    url(r'^data/providers/$', csrf_exempt(views.DataProviders.as_view()), name='data-providers'), #Todo: Remove csrf_excempt before we go life.
    url(r'^data/save/(?P<result_token>[0-9A-Za-z\-]+)/$', views.DataSave.as_view(), name='data-save'),
    url(r'^data/share/(?P<result_token>[0-9A-Za-z\-]+)/$', views.DataShare.as_view(), name='data-share'),
    url(r'^data/results/(?P<result_token>[0-9A-Za-z\-]+)/$', csrf_exempt(views.DataResults.as_view()), name='data-results'),#Todo: Remove csrf_excempt before we go life.
    url(r'^data/results/(?P<result_token>[0-9A-Za-z\-]+)/(?P<result_provider>[0-9A-Za-z\-]+)/$', csrf_exempt(views.DataResultsProvider.as_view()), name='data-results-provider'),
    #url(r'^api/', include(router.urls)),
    #url(r'^api/', include(urlpatterns)),
]
