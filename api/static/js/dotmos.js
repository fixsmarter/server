$(function () {

  var results = [],
    timeoutClosure,
    resultToken,
    newSearch,
    wall = new freewall("#freewall");
  
  wall.fitWidth();


  var submitQuery = function(queryString) {
    $.ajax({
      type: 'POST',
      url: '/api/v1/data/search/',
      data: {
          search: queryString,
          csrfmiddlewaretoken: '{{ csrf_token }}',
          providers: ['all'] // For now, all is default
      },
      success: function (data) {
        clearTimeout(timeoutClosure);
        results = [];
        getResults(data);
      },
    });
  };

  var getResults = function(d){
    resultToken =  (typeof d['result-token'] === 'undefined') ? resultToken : d['result-token'];
    // Get results using token:
    $.ajax({
        type: 'GET',
        url: '/api/v1/data/results/' + resultToken + '/',
        success: function (data) {
          console.log(data);
          addNewData(data, resultToken);    
        },
    });
  };

  var addNewData = function(response, token){
    timeoutClosure = setTimeout(function () {
      getResults(token);
    }, 3000);
    response.results.forEach(function(obj){
      results.push(obj);
    });
    displayResults();
  };

  var displayResults = function() {
    var $output = $(document.createElement('div')).attr('class', 'container'),
      $freewall = $(document.createElement('div')).attr('id', 'freewall'),
      innerHTML = [];
  
    results.forEach(function(obj) {
      var providerUrl;
      var image;
      if(obj.provider === 'twitter'){
        providerUrl = 'http://icons.iconarchive.com/icons/graphics-vibe/simple-rounded-social/256/twitter-icon.png';
      }else if (obj.provider === 'facebook'){
        providerUrl = 'http://icons.iconarchive.com/icons/graphics-vibe/simple-rounded-social/256/twitter-icon.png';
      }else{
        providerUrl = '';
      }
      image = '<img src="' + providerUrl + '" width="40px" style="float:left;">';

      innerHTML.push(
          '<div class="item well">'+ obj.provider +
          '<p>' + image + '</p>' +
          '<p>' + obj.text + '</p>' +
          '<a href="' + obj.url + '">' + obj.url + '</a>' +
          '<h6> Matched Terms: </h6><br />' + obj.matched_terms +
          '</div>'
      );
    });
    $freewall.html(innerHTML.join(''));
    $output.html($freewall);
    console.log('appending data');
    $("#placeholder").html($output);
  };

  $("#search-form").on('submit', function (event) {
      // Stop form from submitting normally
      event.preventDefault();
      submitQuery($("#search-box").val());
  });

      
});
