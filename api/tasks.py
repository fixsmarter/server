from __future__ import absolute_import

from server.celery import app
from data.models import Search, Result, ProviderStatus

import requests

# from providers.provider import TwitterProvider
# from providers.provider import PinterestProvider
# from providers.provider import InstagramProvider
# from providers.provider import YoutubeProvider
# from providers.provider import GoogleProvider
# from providers.provider import WikipediaProvider
# from providers.provider import WolframProvider
# from providers.provider import FacebookProvider
# from providers.provider import LinkedInProvider
# from providers.provider import FlickrProvider

import json

# @app.task(bind=True)
# def task_search_flickr(self, token, search_terms):
#     print "Searching Flickr"
#     flickr = FlickrProvider()
#     if flickr.authenticate():
#         flickr.search(token, search_terms)
#     return True
#
# @app.task(bind=True)
# def task_search_facebook(self, token, search_terms):
#     print "Searching Facebook - Not Implemented"
#     facebook = FacebookProvider()
#     if facebook.authenticate():
#         facebook.search(token, search_terms)
#     return True
#
# @app.task(bind=True)
# def task_search_linkedin(self, token, search_terms):
#     print "Searching LinkedIn - Not Implemented"
#     linkedin = LinkedInProvider()
#     if linkedin.authenticate():
#         linkedin.search(token, search_terms)
#     return True
#
# @app.task(bind=True)
# def task_search_wolfram(self, token, search_terms):
#     print "Searching Wolfram"
#     wolfram = WolframProvider()
#     if wolfram.authenticate():
#         wolfram.search(token, search_terms)
#     return True
#
# @app.task(bind=True)
# def task_search_wikipedia(self, token, search_terms):
#     print "Searching Wikipedia"
#     wikipedia = WikipediaProvider()
#     if wikipedia.authenticate():
#         wikipedia.search(token, search_terms)
#     return True
#
# @app.task(bind=True)
# def task_search_youtube(self, token, search_terms):
#     print "Searching Youtube"
#     youtube = YoutubeProvider()
#     if youtube.authenticate():
#         youtube.search(token, search_terms)
#     return True
#
# @app.task(bind=True)
# def task_search_pinterest(self, token, search_terms):
#     print "Searching Pinterest"
#     pinterest = PinterestProvider()
#     if pinterest.authenticate():
#         pinterest.search(token, search_terms)
#     return True
#
# @app.task(bind=True)
# def task_search_twitter(self, token, search_terms):
#     print "Searching Twitter"
#     twitter = TwitterProvider()
#     if twitter.authenticate():
#         twitter.search(token, search_terms)
#     return True
#
# @app.task(bind=True)
# def task_search_google(self, token, search_terms):
#     print "Searching Google"
#     google = GoogleProvider()
#     if google.authenticate():
#         google.search(token, search_terms)
#     return True
#

@app.task(bind=True)
def task_search_provider(self, provider, token, search_terms):
    print "Searching %s" % provider
    try:
        search_provider = eval("%sProvider()" % provider)
        if search_provider.authenticate():
            search_provider.search(token, search_terms)
    except Exception, e:
        print e
    return True


@app.task(bind=True)
def task_search(self, providers, search_string):
    #
    # Create Search
    #
    search = Search()
    search.token = self.request.id
    search.terms = search_string
    search.save()
    #
    #
    #
    print "Searching Providers"
    """
    for provider in providers:
        print "- %s" % provider
        print "  task_search_provider.delay('%s', search.token, search.terms)" % provider
        try:
            job_id = eval("task_search_provider.delay('%s', search.token, search.terms)" % provider)
            search_provider = ProviderStatus()
            search_provider.job = job_id
            search_provider.name = provider
            search_provider.search = search
            search_provider.save()
        except NameError, e:
            print "---------------------------------- Error ----------------------------------", e
        """
    return True

