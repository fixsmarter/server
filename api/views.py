from celery.backends.base import DisabledBackend
from rest_framework import authentication, permissions, viewsets, views
from rest_framework import status
from rest_framework import decorators
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie

from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.twitter.views import TwitterOAuthAdapter
from allauth.socialaccount.providers.linkedin.views import LinkedInOAuthAdapter
from allauth.socialaccount.providers.instagram.views import InstagramProvider

# twitter, facebook, youtube, pinterest, g+, instagram

from rest_auth.registration.views import SocialLogin

from celery.result import AsyncResult

from tasks import task_search
from data.models import Search, Provider

import json, time, requests

class DefaultsMixin(object):
    """Default settings for view authentication, permissions, filtering and pagination."""

    authentication_classes = (
        authentication.BasicAuthentication,
        authentication.TokenAuthentication,
    )
    permission_classes = (
        permissions.IsAuthenticated,
    )
    paginate_by = 25
    paginate_by_param = 'page_size'
    max_paginate_by = 100


#
# Define the social auth endpoints
#
class FacebookLogin(SocialLogin):
    adapter_class = FacebookOAuth2Adapter


class GoogleLogin(SocialLogin):
    adapter_class = GoogleOAuth2Adapter


class TwitterLogin(SocialLogin):
    adapter_class = TwitterOAuthAdapter


class LinkedInLogin(SocialLogin):
    adapter_class = LinkedInOAuthAdapter


class InstagramLogin(SocialLogin):
    adapter_class = InstagramProvider


class DataProviders(views.APIView):
    def get(self, request, format=None):
        response = [{'name':provider.name, 'description':provider.description, 'url': provider.url, 'image': provider.image, 'enabled': provider.enabled} for provider in Provider.objects.all()]
        return Response(response, status=201)

#
# Define the Data related endpoints.
#
class DataSearch(views.APIView):
    # authentication_classes = authentication.

    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        #print "Searching"

        if request.data['providers[]'] == 'all':
            providers = [provider.name for provider in Provider.objects.all().filter(enabled=True)]
        else:
            providers = request.data['providers[]']

        #print "  Providers:",','.join(providers)
        #print "  Query    :", request.data['search']

        result_token = task_search.delay(providers, request.data['search'])

        response = {'result-token': result_token.id}
        #print "  Token    :",result_token.id

        # if request.user.is_authenticated():
        #     # For logged-in users associate the search with them.
        #     pass
        # else:
        #     # For anonymous users just pass back the results token.
        #     pass

        return Response(response, status=201)

    def get(self, request, format=None):
        #print request.query_params
        request = requests.get('http://192.168.0.5:8983/solr/qband/select', params=request.query_params)
        return Response(request.json(), status=201)


class DataResults(views.APIView):
    def get(self, request, result_token=None, format=None):
        #print "Results"
        #print "  Token    :", result_token
        return_data = {'providers':{}}
        #
        # Get the primary search object
        #
        search = Search.objects.get(token=result_token)
        for provider_task in search.provider_jobs.all():
            task = AsyncResult(provider_task.job)
            return_data['providers'][provider_task.name] = {'status':'', 'results':[]}
            return_data['providers'][provider_task.name]['status'] = task.state

        #
        # Get results, 5 each provider at a time.
        #
        for provider in Provider.objects.all().filter(enabled=True):
            for result in search.results.all().filter(returned=False, provider=provider_task.name)[:6]:
                return_data['providers'][provider_task.name]['results'].append({
                        'provider': result.provider,
                        'title': result.title,
                        'description': result.description,
                        'video': result.video,
                        'image': result.image,
                        'url': result.url,
                        'matched_terms': result.matched_terms,
                    })
                result.returned = True
                result.save()

        ##print json.dumps(return_data, indent=4)

        return Response(return_data, status=201)

class DataResultsProvider(views.APIView):
    def get(self, request, result_token=None, result_provider=None, format=None):
        return_data = {
            'providers':[],
            'results':[]
        }
        #
        # Get the primary search object
        #
        search = Search.objects.get(token=result_token)
        #
        #
        #
        for provider in search.provider_jobs.all().filter(provider=result_provider):
            result = AsyncResult(provider.job)
            try:
                state = str(result.state)
            except Exception, e:
                state = 'Done'

            provider = {
                'name': provider.name,
                'status': state
            }
            return_data['providers'].append(provider)

        for result in search.results.all().filter(returned=False):
            result_data = {
                    'provider': result.provider,
                    'text': result.text,
                    'url': result.url,
                    'matched_terms': result.matched_terms,
                }
            return_data['results'].append(result_data)
            result.returned = True
            result.save()
        #print json.dumps(return_data, indent=4)
        return Response(return_data, status=201)


class DataSave(views.APIView):
    def get(self, request, format=None):
        data = []
        return Response(data, status=201)

        # def put(self, request, format=None):
        #    data = []
        #    return Response(data, status=201)


class DataShare(views.APIView):
    # def get(self, request, format=None):
    #    data = []
    #    return Response(data, status=201)

    def post(self, request, format=None):
        data = []
        return Response(data, status=201)
